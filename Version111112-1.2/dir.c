/* ************************************************************************
 *       Filename:  dir.c
 *    Description:  比如文件夹名称为: 文浩 99,对这样的一系统文件名进行统计.
 *        Version:  1.0
 *        Created:  2011年08月25日 15时03分30秒
 *       Revision:  none
 *       Compiler:  gcc
 *         Author:  wenhao 
 *        Company:  SunplusApp
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 修改者:文浩
 时间:2011/10/10
 版本:V1.1
 升级内容:1.添加注释
		  2.对当前目录下的.exe和.doc文件不进行统计
		  3.将成绩写入TXT顺序改为从高分到低分
 * ************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int len = 0;
char *list[1000];
char buf[100];

//函数声明
void getlist();
void BubbleSort(char *IntArray[],int ArrayLen) ;
void SwapData(char *a,char *b);

int main(int argc,char *argv[])
{
	int i;
	int fd;
	getlist(); // //获取当前目录所存在的文件名称,并进行排序
	fd = open("./成绩排名.txt",O_CREAT|O_RDWR,0666); //创建"成绩排名.txt"文件
	if(fd < 0)
	{
		printf("open error\n");
		exit(-1);
	}
	
	dup2(fd,fileno(stdout)); //标准输出重定向,打印到fd文件中
	
	for(i = len-1; i >= 0; i--)
	{
		printf("%s\n",list[i]);
		free(list[i]);
	}
	
	close(fd); //关闭文件
	return 0;
}

void getlist()
{
	DIR *dir;
	struct dirent *ptr;
	int i = 0;
	char *filenamept = NULL;
	dir = opendir(".");//打开当前文件夹
	if(dir == NULL)
	{
		printf("no this dir\n");
		exit(-1);
	}
	
	while( (ptr = readdir(dir)) != NULL)//获取当前目录所存在文件名称
	{
		if( (ptr->d_name[0] == '.') || \
			(strstr(ptr->d_name,".doc") != 0 ) || \
			(strstr(ptr->d_name,".exe") != 0 ) || \
			(strstr(ptr->d_name,".txt") != 0 ) 
		  )
		{
			continue;
		}
		filenamept = (char *)malloc(sizeof(buf));
		strcpy(filenamept,ptr->d_name);
		list[i++] = filenamept;
	}
	
	len = i;
	closedir(dir);
	
	BubbleSort(list,len);//对目录内容进行排序
}

//冒泡法进行排序
void BubbleSort(char *IntArray[],int ArrayLen)   
{   
	int i,j;
	int tempa,tempb;

    for(i = 0;i < ArrayLen;i++)   
    {   
        for(j = 0;j < ArrayLen-i-1;j++)   
        {   

			sscanf(IntArray[j],"%*s %d",&tempa);
			sscanf(IntArray[j + 1],"%*s %d",&tempb);
            if(tempa > tempb)   
            {   
                SwapData(IntArray[j],IntArray[j+1]);   
            }   
        }   
    }  

}  
//字符串交换
void SwapData(char *a,char *b)
{   
	char iTemp[100] = "";

    strcpy(iTemp,a);  
    strcpy(a,b); 
    strcpy(b,iTemp); 
}

