/* ************************************************************************
 *       Filename:  dir.c
 *    Description:  
 *        Version:  1.0
 *        Created:  2011年08月25日 15时03分30秒
 *       Revision:  none
 *       Compiler:  gcc
 *         Author:  YOUR NAME (), 
 *        Company:  
 * ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

char *q = NULL;
int len = 0;
char *list[1000];
char buf[100];

void getlist();

int main(int argc,char *argv[])
{
	int i;
	int fd;
	getlist();
	fd = open("./test.txt",O_CREAT|O_RDWR,0666);
	if(fd < 0)
	{
		printf("open error\n");
		exit(-1);
	}
	
	dup2(fd,1);
	
	for(i = 0;i < len; i++)
	{
		printf("%s\n",list[i]);
		free(list[i]);
	}
	
	return 0;
}

void getlist()
{
	DIR *dir;
	struct dirent *ptr;
	int i = 0;
	dir = opendir(".");
	if(dir == NULL)
	{
		printf("no this dir\n");
		exit(-1);
	}
	
	while( (ptr = readdir(dir)) != NULL)
	{
		if(ptr->d_name[0] == '.')
		{
			continue;
		}
		q = (char *)malloc(sizeof(buf));
		strcpy(q,ptr->d_name);
		list[i++] = q;
	}
	len = i;
	closedir(dir);
}

